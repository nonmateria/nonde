function fish_prompt --description 'Write out the prompt'
	set -l last_status $status

    printf "%s|:::|%s " (set_color red --bold)  (set_color normal)

    set -l color_cwd
    set -l suffix
    switch "$USER"
        case root toor
            if set -q fish_color_cwd_root
                set color_cwd $fish_color_cwd_root
            else
                set color_cwd $fish_color_cwd
            end
            set suffix '#'
        case '*'
            set suffix '>'
    end

    echo -n -s (set_color $color_cwd) (prompt_pwd)  "$suffix "
end
