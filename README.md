nonde : nondesktop environment 
==============
This is basically a dotfile repo, a collection of files to set up my usual desktop enviroment (well, not based on the desktop metaphor) and a collection of scripts that i keep in my `~/bin`. It uses a Debian 10 netinst as starting point.

The current setup is based on:

* shell: [fish](https://fishshell.com/)
* window manager: openbox
* bar: tint2
* system monitor: conky
* launcher: rofi
* terminal: terminator 
* text editor: [micro](https://micro-editor.github.io/) 
* c compiler : clang 
* file manager: thunar
* network management : network-manager
* power management : xfce4-power-manager

## software catalog

Graphics :

* graphic scripting : [nsketch](https://codeberg.org/npisanti/nsketch)
* small tools : [scriptsuite](https://codeberg.org/npisanti/scriptsuite)
* graphic editing : krita 
* photo postprocessing : darktable 
* scanning : xsane
* video editing : kdenlive

Audio :

* sequencer : [orca-c](https://github.com/hundredrabbits/orca-c)
* sampler synth : [folderkit](https://codeberg.org/npisanti/folderkit)
* audio from code : csound 
* additional synth/fx : VCV Rack 
* sample editor : audacity 

Web :

* web browser: firefox-esr 
* rss feeds : liferea 
* e-mail : thunderbird 
* instant messaging : telegram-desktop
* live streaming : obs-studio

Utility :

* video player : mpv 
* audio player : cmus
* image viewer : gpicview
* multi screen control : arandr 
* wallpaper management : nitrogen 
  
The included theme has been generated with [oomox](https://github.com/themix-project/oomox).

The setup scripts are perennial alpha so DO NOT USE, just pick the confs you like.   

Here is an example of the desktop, i mostly use 1px pixelperfect lines, click for fullscreen, illustration by Tsutomu Nihei   
[![alt tag](http://nonmateria.com/data/repos/nonde.png)](http://nonmateria.com/data/repos/nonde.png)

Everything released into public domain.

Big thanks go to [crunchbang](https://en.wikipedia.org/wiki/CrunchBang_Linux), from which my own setup spawned as a personal take.
