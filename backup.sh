#!/usr/bin/fish

set host (cat /proc/sys/kernel/hostname)

rm -rf ~/system/nonde/confs/*

mkdir -p ~/system/nonde/confs/$host

cp -ar ~/.config/terminator ~/system/nonde/confs/
cp -ar ~/.config/openbox/ ~/system/nonde/confs/
cp -ar ~/.config/openbox/autostart ~/system/nonde/confs/$host/autostart
cp -ar ~/.config/fish/ ~/system/nonde/confs/
cp ~/.config/tint2/tint2rc ~/system/nonde/confs/tint2rc
cp ~/.Xresources confs/Xresources
cp ~/.conkyrc ~/system/nonde/confs/$host/conkyrc
cp ~/.gdbinit ~/system/nonde/confs/gdbinit
cp ~/.config/compton.conf ~/system/nonde/confs/compton.conf
cp ~/.bashrc ~/system/nonde/confs/bashrc
cp ~/.xbindkeysrc ~/system/nonde/confs/xbindkeysrc
cp ~/.local/share/applications/mimeapps.list ~/system/nonde/confs/mimeapps.list

mkdir -p ~/system/nonde/confs/geany 
cp ~/.config/geany/colorschemes/less.conf ~/system/nonde/confs/geany/less.conf
cp ~/.config/geany/geany.conf ~/system/nonde/confs/geany/geany.conf
cp ~/.config/geany/keybindings.conf ~/system/nonde/confs/geany/keybindings.conf
sed -i -r 's/(recent_files=).*/\1/' ~/system/nonde/confs/geany/geany.conf 
sed -i -r 's/(recent_projects=).*/\1/' ~/system/nonde/confs/geany/geany.conf 
sed -i -r 's/(current_page=).*/\1/' ~/system/nonde/confs/geany/geany.conf 
sed -i '/current_page/q' ~/system/nonde/confs/geany/geany.conf

cp ~/.config/redshift.conf ~/system/nonde/confs/redshift.conf
cp ~/.config/bl-exit/bl-exitrc ~/system/nonde/confs/
cp ~/.gtkrc-2.0 ~/system/nonde/confs/gtkrc-2.0

cp ~/.config/mpv/mpv.conf ~/system/nonde/confs/mpv.conf

cp -ar ~/.config/micro/ ~/system/nonde/confs/
rm ~/system/nonde/confs/micro/buffers/history
rm -rf ~/system/nonde/confs/micro/backups

cp ~/.tmux.conf ~/system/nonde/confs/tmux.conf

#cp ~/.config/cmus/rc ~/system/nonde/confs/cmusrc

exit
