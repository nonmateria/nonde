#!/usr/bin/fish

set filename (find ~/resources/refs -name "*jpg" | shuf -n 1 )
echo "copying $filename to clipboard"
xclip -selection clipboard -target image/jpg -i "$filename"

exit
