#!/usr/bin/fish

set b0 ( acpi -b | grep "Battery 0" )

set eta ( echo "$b0" | awk '{print $5}' | sed 's/,//g' )
set not ( echo "$b0" | awk '{print $3}' | sed 's/,//g' )
set pct ( echo "$b0" | awk '{print $4}' | sed 's/,//g' )
set bat_status ( echo "$b0" | awk '{print $3 " " $4}' | sed 's/,//g' )

if test "$eta" = "discharging" || test "$not" = "Not"
  set bat_status ">|<  $eta"
else if test "$eta" = "until" 
  set bat_status ">|< 100%"  
else if test "$not" = "Full" || test "$not" = "Unknown"
  set bat_status ">|<  $pct"
else if test "$not" = "Charging" 
  set bat_status ">>>  $pct"
else if test "$not" = "Discharging" 
  set bat_status "<<<  $pct"
end

echo "$bat_status"

exit
