#!/usr/bin/fish

if test -z "$argv[1]"
	cd ~/lab/deck
else
	cd "$argv[1]"
end

set INPUT (ls | rofi -dmenu -p "right card: " -theme ~/system/nonde/extra/themes/less.rafi)

wmctrl -c "deck_right"

nsk "$INPUT" -R &

exit
