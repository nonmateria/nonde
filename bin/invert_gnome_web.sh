#!/usr/bin/fish
set INVERTED (dconf read /org/gnome/epiphany/web/enable-user-css)

if test "$INVERTED" = "false" 
	dconf write /org/gnome/epiphany/web/enable-user-css true	
	echo "set to true"
else
	dconf write /org/gnome/epiphany/web/enable-user-css false
	echo "set to false" 
end

exit
