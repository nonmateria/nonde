#!/usr/bin/fish

set DATE (date +%Y%m%d)
set TIME (date +%Hh%M)

# Start recording audio 
jack_capture -b 24 $HOME/Screencasts/screencast_audio_$DATE-$TIME.wav 

exit 