#!/usr/bin/fish

set filename (find ~/resources/refs -name "*jpg" | shuf -n 1 )

gpicview $filename &

sleep 0.2

flameshot gui &

sleep 0.2

xdotool key Control_L+a

sleep 0.1

xdotool key p

exit
