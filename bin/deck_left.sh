#!/usr/bin/fish

if test -z "$argv[1]"
	cd ~/lab/deck
else
	cd "$argv[1]"
end

set INPUT (ls | rofi -dmenu -p "left card: " -theme ~/system/nonde/extra/themes/less.rafi)

wmctrl -c "deck_left"

nsk "$INPUT" -L &

exit
