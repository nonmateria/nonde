#!/bin/sh

# thinkpad 
xrandr --output VIRTUAL1 --off --output DP3 --off --output DP2 --off --output DP1 --off --output HDMI3 --off --output HDMI2 --off --output HDMI1 --off --output LVDS1 --mode 1600x900 --pos 0x0 --rotate normal --output VGA1 --off

xcalib -d :0 /home/nick/system/nonde/machines/thinkpad_t430/N140FGE_EA2.icm

# zenbook 
#xrandr --output eDP1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP1 --off --output HDMI1 --off --output HDMI2 --off --output VIRTUAL1 --off

#xcalib -d :0 /home/nicola/system/nonde/machines/asus_zenbook_ux303LA/icc/UX303LA_8086_AE0D1361.icm


nitrogen --restore

killall conky 

conky &

exit
