#!/usr/bin/fish

set VIDEO ( pgrep ffmpeg )
set AUDIO ( pgrep jack_capture )

if test "$VIDEO" != "" && test "$AUDIO" != ""
    echo "Audio/Video"
else if test "$VIDEO" != ""
    echo "Video"
else if test "$AUDIO" != ""
    echo "Audio"
else
    echo "Nothing"
end

exit
