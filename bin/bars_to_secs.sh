#!/usr/bin/fish

if test -z "$argv[1]"; or  test -z "$argv[2]"
    echo "USAGE : bars_to_secs.sh [bars] [bpm]"
    exit
end

set bars $argv[1]
set bpm $argv[2]

set sec (math "(60*$bars) / ($bpm/4)" )

echo "$bars bars at $bpm bpm last $sec seconds";

exit