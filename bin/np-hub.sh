#!/usr/bin/fish
# usage: np-hub.sh

# avoid recording password
set fish_history ""

set pass (read -s -P "insert hub password: " ) 
echo ""
sudo create_ap -n --no-virt wlp3s0 np-hub $pass
exit
