#!/usr/bin/fish
# usage: np-hub.sh PASSWORD

# avoid recording password
set fish_history ""

set pass (read -s -P "insert spot password: " ) 
echo ""
sudo create_ap -n --no-virt wlp2s0 np-hotspot $pass
exit
