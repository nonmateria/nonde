#!/usr/bin/fish

set VIDEO ( pgrep ffmpeg )
set AUDIO ( pgrep jack_capture )

set DATE (date +%Y%m%d)
set TIME (date +%Hh%M)

if test "$VIDEO" != "" && test "$AUDIO" != ""
    killall ffmpeg 
    killall jack_capture
    sleep 2
    ffmpeg -i ~/Videos/screencast_temp.mkv -i ~/Videos/screencast_temp.wav -c:v copy -c:a libvorbis -shortest ~/Videos/screencast_$DATE-$TIME.mp4
else if test "$VIDEO" != ""
    killall ffmpeg 
    sleep 2  
    ffmpeg -i ~/Videos/screencast_temp.mkv -codec copy ~/Videos/screencast_$DATE-$TIME.mp4 
else if test "$AUDIO" != ""
    killall jack_capture 
    sleep 2  
    mv ~/Videos/screencast_temp.wav ~/Videos/jack_recording_$DATE-$TIME.wav
end

exit
