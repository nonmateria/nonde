#!/usr/bin/fish

set DEVICE "U192k"

if test -n "$argv[1]"
    set DEVICE "$argv[1]"
end

jackd -R -P89 -p64 -t2000 -dalsa -dhw:$DEVICE -r44100 -p512 -n2 -s -P &

sleep 2 
echo "---------------------------------"
echo "press 'q' to quit the JACK server"

while true
    set k (read -n 1 -P ">" ) 

	if test "$k" = "q"
		killall jackd
		exit
	end		
	echo "nope, it's 'q' to quit" 
end

exit
