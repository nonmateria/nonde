#!/usr/bin/fish

set paths "system/nonde" "htdocs/nonmateria-ssg" "system/tools/nsketch" "system/tools/folderkit" "system/tools/scriptsuite" "lab/orca/repos/orca-snippets" "lab/scrapbook" "lab/deck"

set nothingtocommit "true"

for i in $paths
	cd "$HOME/$i"
	set repo_status (git status --ignore-submodules | sed -n 4p)
	if test "$repo_status" = "  (use \"git push\" to publish your local commits)" 
		echo "> $i"
		set nothingtocommit "false"
	else if test "$repo_status" != "nothing to commit, working tree clean"
		echo "$i"
		set nothingtocommit "false"
	end
end

if test "$nothingtocommit" = "true"
    echo "nothing to commit"
end

exit
