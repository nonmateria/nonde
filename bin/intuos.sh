#!/bin/sh
xsetwacom set "Wacom Intuos BT M Pad pad" Button 1 "5"
xsetwacom set "Wacom Intuos BT M Pad pad" Button 2 "4"
xsetwacom set "Wacom Intuos BT M Pad pad" Button 3 "key +ctrl z -ctrl"
xsetwacom set "Wacom Intuos BT M Pad pad" Button 8 "key +ctrl +shift z -ctrl -shift"
xsetwacom set "Wacom Intuos BT M Pen stylus" CursorProximity 60

# default area values are 0 0 21600 13500
#xsetwacom set "Wacom Intuos BT M Pen stylus" Area 0 0 43200 27000
#xsetwacom set "Wacom Intuos BT M Pen stylus" Mode "Relative"
#xsetwacom set "Wacom Intuos BT M Pen stylus" Rotate none 

# rotated setup
xsetwacom set "Wacom Intuos BT M Pen stylus" Mode "Relative"
xsetwacom set "Wacom Intuos BT M Pen stylus" Rotate ccw 
xsetwacom set "Wacom Intuos BT M Pen stylus" Area 0 0 27000 43200

# default
#xsetwacom set "Wacom Intuos BT M Pen stylus" Rotate none 
#xsetwacom set "Wacom Intuos BT M Pen stylus" Mode "Absolute"
#xsetwacom set "Wacom Intuos BT M Pen stylus" Area 0 0 21600 13500

exit
