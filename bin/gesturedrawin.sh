#!/usr/bin/fish

set TIME "60"

set SECS ( math "60 * 30" )

if test -n "$argv[1]"
    set TIME "$argv[1]"
end

if test -n "$argv[2]"
    set SECS ( math "60 * $argv[2]" )
end

echo "---------------------------------"
echo "press 'CTRL+C' to quit gesture drawing"

while test $SECS -gt 0 
	set filename (find ~/resources/refs -name "*jpg" | shuf -n 1 )
	gpicview $filename &
	
	set tminus (math "$TIME - 10" ) 
	sleep $tminus
	
	notify-send "GESTURE: 10 second remaining"
	sleep 5 
	notify-send "GESTURE: 5 second remaining"
	sleep 5
	
	killall gpicview
    
	set SECS ( math "$SECS - $TIME" )

	echo "remaining session time: $SECS seconds"
	sleep 0.5 
end

exit
