#!/usr/bin/fish
amixer set Master toggle

set mutestatus (amixer sget Master | grep 'Mono:' | awk -F'[][]' '{ print $6 }')

if test "$mutestatus" = "on"
    notify-send "audio active"
else
    notify-send "audio muted"
end

exit
