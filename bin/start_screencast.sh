#!/usr/bin/fish

# fullscreen
#ffmpeg -y -f x11grab -s 1920x1080 -i :0.0+0,0 -vcodec libx264 -threads 2 $HOME/Videos/screencast_temp.mkv &

#ffmpeg -y -f x11grab -s 1600x900 -i :0.0+0,0 -vcodec libx264 -threads 2 $HOME/Videos/screencast_temp.mkv &

# orca fullscreen
#ffmpeg -y -f x11grab -s 1280x720 -i :0.0+315,130 -vcodec libx264 -threads 2 $HOME/Videos/screencast_temp.mkv &

ffmpeg -y -f x11grab -s 800x800 -i :0.0+0,0 -vcodec libx264 -threads 2 $HOME/Videos/screencast_temp.mkv &

#ffmpeg -y -f x11grab -s 1080x740 -i :0.0+420,80 -vcodec libx264 -threads 2 $HOME/Videos/screencast_temp.mkv &

jack_capture -d 6000 $HOME/Videos/screencast_temp.wav &

exit
