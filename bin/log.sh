#!/usr/bin/fish
set TASK $argv[1]
set START ( date )

#deactivate internet
nmcli r wifi off 

echo ">>> $START, working on $TASK"

echo "press 'q' if your're done"
while true
    set k (read -n 1 -P ">" ) 

	if test "$k" = "q"
		set END ( date )
		set LOG "$START,$END,$TASK"
		echo -e "$LOG" >> ~/log.csv
		# reactivate internet 
		nmcli r wifi on
		echo ">>> $END, finished $TASK"
		exit
	end		
	echo "nope, it's 'q'" 
end

exit
