#!/usr/bin/fish

# asus zenbook ux303la
set mhz (cat /sys/class/drm/card0/gt_cur_freq_mhz)
set bar (math "($mhz-200) / 8.0") 

# thinkpad t430
#set mhz (cat /sys/class/drm/card0/gt_cur_freq_mhz)
#set bar (math "($mhz-350) / 8.5") 

echo "$bar"

exit
