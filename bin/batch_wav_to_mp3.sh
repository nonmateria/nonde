#!/usr/bin/fish

SAVEIF=$IFS

IFS=$(echo -en "\n\b")

for file in *.wav
	set name ( basename $file .wav )
	lame -V0 -h -b 320 -cbr $file $name.mp3
end

IFS=$SAVEIF

exit 
