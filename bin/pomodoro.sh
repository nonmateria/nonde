#!/usr/bin/fish
set MINUTES 40
set TASK $argv[1]
set START ( date )
set DURATION ( math "$MINUTES*60" )
#deactivate internet
nmcli r wifi off 
notify-send "$START, working on $TASK"
sleep  $DURATION
#floods the screen 
for n in (seq 1 80) 
	notify-send "pomodoro ended, take a break"
end
set END ( date )
set LOG "$START,$END,$TASK"
echo -e "$LOG" >> ~/log.csv
# reactivate internet 
nmcli r wifi on
exit
