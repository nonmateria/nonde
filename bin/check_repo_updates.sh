#!/usr/bin/fish

set paths "apps/Dotgrid" "apps/create_ap" "apps/FlameGraph" "apps/ardour"

for i in $paths
    cd "$HOME/$i"
    git fetch -q
    set repo_status (git status | sed -n 2p)
    
    if test "$repo_status" != "Your branch is up to date with 'origin/master'."
        echo "[ $i ] $repo_status"
   	end
end

exit
