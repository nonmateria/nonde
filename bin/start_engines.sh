#!/usr/bin/fish

set DEVICE "MK2"

# to get cards use: cat /proc/asound/cards

if test -n "$argv[1]"
    set DEVICE "$argv[1]"
end

sudo cpufreq-set -c 0 -g performance
sudo cpufreq-set -c 1 -g performance
sudo cpufreq-set -c 2 -g performance
sudo cpufreq-set -c 3 -g performance

set rspid (pgrep redshift) 
if test "$rspid" != "" 
    pkill -USR1 redshift
    pkill redshift
end

set jack_open (pstree | grep jackd)
if test "$jack_open" = ""
    jackd -R -P89 -p64 -t2000 -dalsa -dhw:$DEVICE -r44100 -p512 -n2 -s -P &
    sleep 2
end

folderkit ~/resources/folderkit 

exit
