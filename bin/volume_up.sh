#!/usr/bin/fish
amixer set Master 1+
set volume (amixer sget Master | grep 'Mono:' | awk -F'[][]' '{ print $2 }')
set volume "volume = $volume"
notify-send "$volume"
exit
