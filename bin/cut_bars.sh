#!/usr/bin/fish

# TODO: add optional offset in 16ths1
# TODO: optional crossfading? 

if test -z "$argv[1]"; or  test -z "$argv[2]"; or  test -z "$argv[3]"
    echo "USAGE : cut_bars.sh [file] [bars] [bpm]"
    echo "silence before start will be trimmed" 
    exit
end

set file $argv[1]
set bars $argv[2]
set bpm $argv[3]

set base ( basename $file .wav ) 
set outfile $base"_"$bars"b.wav"

set sec (math "(60*$bars) / ($bpm/4)" )

sox $file temp.wav silence 1 0.0 0%
sox temp.wav $outfile trim 0 $sec
rm temp.wav 

exit