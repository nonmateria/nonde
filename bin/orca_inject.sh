#!/usr/bin/fish

if test -z "$argv[1]"
	cd ~/lab/orcasets/inject/templates
else
	cd "$argv[1]"
end

# needed to hide the mouse pointer
xdotool mousemove 5000 5000

set INPUT (ls *.orca | rofi -dmenu -p "inject: " -theme ~/lab/orcasets/inject/extra/inject.rafi)
 
xclip -sel c -i $INPUT

# paste
xdotool key Control_L+v

sleep 0.05

# reduce selected area
xdotool key Escape

exit
