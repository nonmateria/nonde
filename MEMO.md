
folders semantics:
- `bureau` is for legal documents, invoices and bills
- `lab` : is for working on projects
- `library` : books, movies, music, etc
- `htdocs/nonmateria` is where the local website is 
- `resources` : is for image or audio assets  
- `system` : applications and configs
- `warehouse` is the finished projects archive 
