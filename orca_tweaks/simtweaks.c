
// change this operators into sim.c to have flashing bangs 

BEGIN_OPERATOR(delay)
  LOWERCASE_REQUIRES_BANG;
  PORT(0, -1, IN | PARAM);
  PORT(0, 1, IN);
  Usz rate = index_of(PEEK(0, -1));
  Usz mod_num = index_of(PEEK(0, 1));
  if (rate == 0)
    rate = 1;
  if (mod_num == 0)
    mod_num = 8;
  if (Tick_number % (rate * mod_num) == 0) {
    PORT(1, 0, OUT);
    POKE(1, 0, '*');
  } else {
    PORT(1, 0, PARAM);
    POKE(1, 0, '.');
  }
END_OPERATOR

BEGIN_OPERATOR(if)
  LOWERCASE_REQUIRES_BANG;
  PORT(0, -1, IN | PARAM);
  PORT(0, 1, IN);
  Glyph g0 = PEEK(0, -1);
  Glyph g1 = PEEK(0, 1);
  if (g0 == g1) {
    PORT(1, 0, OUT);
    POKE(1, 0, '*');
  } else {
    PORT(1, 0, PARAM);
    POKE(1, 0, '.');
  }
END_OPERATOR

BEGIN_OPERATOR(uclid)
  LOWERCASE_REQUIRES_BANG;
  PORT(0, -1, IN | PARAM);
  PORT(0, 1, IN);
  Glyph left = PEEK(0, -1);
  Usz steps = 1;
  if (left != '.' && left != '*')
    steps = index_of(left);
  Usz max = index_of(PEEK(0, 1));
  if (max == 0)
    max = 8;
  Usz bucket = (steps * (Tick_number + max - 1)) % max + steps;
  if (bucket >= max) {
    PORT(1, 0, OUT);
    POKE(1, 0, '*');
  } else {
    PORT(1, 0, PARAM);
    POKE(1, 0, '.');
  }
END_OPERATOR
