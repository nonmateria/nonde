#!/bin/sh

cd ~

echo " -------- getting samples --------"
wget -r -m --no-parent -erobots=off --reject="index.html*" http://nonmateria.com/samples/

echo "-------- unzipping --------"
cd ~/npisanti.com/samples/
unzip '*.zip'

echo "-------- removing archives --------"
rm *.zip

echo "-------- moving data --------"
mkdir -p ~/resources
mv ~/nonmateria.com/samples/ ~/resources/nm-samples
rmdir ~/nonmateria.com/

echo "-------- done  --------"

exit
