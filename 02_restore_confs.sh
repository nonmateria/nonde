#!/usr/bin/fish

set host (cat /proc/sys/kernel/hostname)

# restoring confs
echo "restoring confs"
mkdir -p ~/.config/terminator/
cp -ar ~/system/nonde/confs/terminator/config ~/.config/terminator/config
cp -ar ~/system/nonde/confs/openbox ~/.config
cp ~/system/nonde/confs/$host/autostart ~/.config/openbox/autostart 

mkdir -p ~/.config/tint2
cp ~/system/nonde/confs/tint2rc ~/.config/tint2/tint2rc
cp ~/system/nonde/confs/$host/conkyrc ~/.conkyrc
cp ~/system/nonde/confs/gdbinit ~/.gdbinit
cp ~/system/nonde/confs/compton.conf ~/.config/compton.conf 
cp ~/system/nonde/confs/bashrc ~/.bashrc 
cp ~/system/nonde/confs/xbindkeysrc ~/.xbindkeysrc 
cp ~/system/nonde/confs/Xresources ~/.Xresources 

cp -ar ~/system/nonde/confs/fish ~/.config

cp ~/system/nonde/confs/gitconfig ~/.gitconfig 

mkdir -p ~/.config/geany 
cp ~/system/nonde/confs/geany/less.conf ~/.config/geany/colorschemes/less.conf 
cp ~/system/nonde/confs/geany/geany.conf ~/.config/geany/geany.conf 
cp ~/system/nonde/confs/geany/keybindings.conf ~/.config/geany/keybindings.conf 

mkdir -p ~/.local/share/applications/
cp ~/system/nonde/confs/mimeapps.list ~/.local/share/applications/mimeapps.list 

cp ~/system/nonde/confs/gtkrc-2.0 ~/.gtkrc-2.0

echo "extra themes"
cp -r ~/system/nonde/extra/themes/* ~/.themes/

echo "launcher icon"
cp -r ~/system/nonde/extra/icons/* ~/.icons/

cp ~/system/nonde/extra/base/thunar.xml ~/.config/xfce4/xfconf/xfce-perchannel-xml/thunar.xml 

mkdir -p ~/.config/mpv
cp ~/system/nonde/confs/mpv.conf ~/.config/mpv/mpv.conf

cp -ar ~/system/nonde/confs/micro ~/.config
cp ~/system/nonde/confs/tmux.conf ~/.tmux.conf

#mkdir -p ~/.config/cmus/
#cp ~/system/nonde/confs/cmusrc ~/.config/cmus/rc

exit
