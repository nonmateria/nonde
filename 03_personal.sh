#!/usr/bin/fish

echo "git config"
git config --global user.name "nonmateria"
git config --global user.email nico@nonmateria.com
git config --global core.editor micro 

echo "installing programs..."

mkdir -p ~/system/apps
mkdir -p ~/system/tools 

echo "installing lua strict module"
sudo mkdir -p /usr/local/share/lua/5.1/
sudo cp  ~/system/nonde/extra/libs/strict.lua  /usr/local/share/lua/5.1/

cd ~/system/apps
git clone https://github.com/brendangregg/FlameGraph.git

echo "getting youtube-dl" 
sudo apt-get install youtube-dl youtubedl-gui

echo "installing njconnect"
cd ~/system/apps
git clone https://github.com/radiganm/njconnect
cd njconnect 
make 

echo "installing midimonster"
cd ~/system/apps
sudo apt-get install libevdev-dev liblua5.3-dev python3-dev
git clone https://github.com/cbdevnet/midimonster
cd midimonster
export PREFIX=$HOME/system/apps
export PLUGINS=$HOME/system/apps/midimonster/backends
export DEFAULT_CFG=$HOME/system/apps/midimonster/monster.cfg
make

echo "installing orca" 
cd ~/system/tools
sudo apt-get install libportmidi-dev libncurses5-dev libncursesw5-dev
git clone https://github.com/hundredrabbits/Orca-c
cd orca
./tool build --nomouse --portmidi orca 

echo "installing folderkit"
sudo apt-get install liblo-dev libjack-dev libfftw3-dev libfftw3-single3 
cd ~/system/tools
git clone https://codeberg.org/nonmateria/folderkit.git
cd folderkit 
make

echo "installing nsketch" 
cd ~/system/tools
sudo apt-get install libv4l-0 libv4l-dev liblo7 liblo-dev jackd1 libjack-dev 
git clone https://codeberg.org/nonmateria/nsketch.git
cd nsketch 
mkdir libs
cd libs 
git clone https://github.com/raysan5/raylib
cd raylib
sudo apt install libasound2-dev mesa-common-dev libx11-dev libxrandr-dev libxi-dev xorg-dev libgl1-mesa-dev libglu1-mesa-dev
make 
sudo make install 
cd ..
git clone https://luajit.org/git/luajit-2.0.git
cd luajit-2.0
git checkout v2.1
make
cd ..
make

echo "getting scriptsuite"
cd ~/system/tools
git clone https://codeberg.org/nonmateria/scriptsuite.git

mkdir -p ~/lab

echo "getting scrapbook" 
cd ~/lab
git clone https://codeberg.org/nonmateria/scrapbook.git

echo "getting bandcamp-dl"
sudo apt-get install python3-pip
pip3 install bandcamp-downloader
set -U fish_user_paths /home/nicola/.local/bin/ $fish_user_paths

echo "installing emulators" 
sudo apt-get install retroarch libretro-mgba libretro-snes9x libretro-nestopia 

exit
