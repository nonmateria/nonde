#/bin/sh

sudo apt install unzip git gdb curl cmake libx11-dev libglu1-mesa-dev libxrandr-dev libxinerama-dev libxcursor-dev libxi-dev zlib1g-dev libasound2-dev libgtk2.0-dev jq

cd ~/system/apps
git clone https://github.com/VCVRack/Rack.git 
cd Rack
git submodule update --init --recursive
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/VCVRack/Fundamental.git
cd Fundamental 
make dep -j4
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/VCVRack/AudibleInstruments.git
cd AudibleInstruments
git submodule update --init --recursive
make dep -j4 
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/VCVRack/Befaco.git
cd Befaco 
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/VCVRack/ESeries.git
cd ESeries
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/ValleyAudio/ValleyRackFree.git
cd ValleyRackFree
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/kauewerner/Axioma.git
cd Axioma
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/algoritmarte/AlgoritmarteVCVPlugin.git
cd AlgoritmarteVCVPlugin
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/clone45/voxglitch.git 
cd voxglitch
make -j4 

cd ~/system/apps/Rack/plugins
git clone https://github.com/RCameron93/FehlerFabrik.git
cd FehlerFabrik
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/jeremymuller/Sha-Bang-Modules.git
cd Sha-Bang-Modules
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/n0jo/rackwindows.git
cd rackwindows
make -j4

cd ~/system/apps/Rack/plugins
git clone https://gitlab.com/unlessgames/unless_modules
cd unless_modules
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/aptrn/stocaudio-modules.git
cd stocaudio-modules
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/AScustomWorks/AS.git
cd AS
make -j4

cd ~/system/apps/Rack/plugins
git clone https://github.com/LomasModules/LomasModules.git
cd LomasModules
make -j4

exit
