## raspberryPi memos
memos for setting up the usual raspberry pi system for rpiezos, gpiosc and installations.

### flash image (linux only)
check drives  
`lsblk`  
  
if needed umount  
`umount /dev/sdx`  
  
zero out the card (optional)   
`$ sudo dd bs=4M if=/dev/zero of=/dev/sdx`  
  
flash the image  
`$ sudo dd bs=4M if=path_to_raspbian_minimal.img of=/dev/sdx`  

### raspi config

`sudo raspi-config  `
>expand filesystem  
>overclock to rPi2  
>set up keyboard layout  
>boot to user without password  
>deactivate wait for network  
>audio through speakers not hdmi  
>128mb to graphics
>interfacing options-->P6 Serial ---> Disable shell / enable serial

(optional)  
`sudo dpkg-reconfigure console-setup`  
>set font to terminus

### update system
```
sudo apt-get clean  
sudo apt-get update
sudo apt-get dist-upgrade
```

### install git and utilities, 
```
sudo apt-get install git nmon omxplayer 
curl https://getmic.ro | bash

```
more tweaks:
```
sudo cp ~/system/nonde/extra/base/limits.conf /etc/security/limits.conf 
sudo apt-get purge bluez 
```

### optimizations for reducing card wearing
remove the card from the rpi, mount it on your laptop and use this command to disable journaling (linux only)  
`sudo tune2fs -O ^has_journal /dev/sdb2`  

then log again into the pi  
```
sudo apt-get remove dphys-swapfile
sudo apt-get autoremove
```

also 
```
sudo nano /etc/fstab
```
add  
```
tmpfs /var/tmp tmpfs nodev,nosuid,size=32M 0 0  
tmpfs /var/log tmpfs nodev,nosuid,size=16M 0 0  
```
(this will make temporary filesystems on the RAM for `/var/log` and `/var/tmp`

( for saving your rpi is also a good idea to use heatsinks )


### create jumper shutdown file

copy `gpio_shutdown.py` to the home folder   
   
then make it executable   
`chmod +x ~/gpio_shutdown.py`
   
   
### copy autorun file
copy `autorun.sh` file to home folder
   
    
### enable autorun and gpio shutdown
now we will activate the autorun routine at startup, and enable safe shutdown by putting a jumper on the 5-6 pins. We will also put an extra command to be shure the video never shuts down  
do  
`sudo nano /etc/rc.local`  
and add  
```sh
#deactivate blanking 
setterm -blank 0 -powerdown 0 -powersave off

#autorun
(sleep 3; su - pi -c "sh /home/pi/autorun.sh" ) &
```   
add to `autorun.sh`
```
	# shutdown with pin 39-40
	sudo python /home/pi/gpio_shutdown.py &
```
