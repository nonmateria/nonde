#!/bin/sh
#xrandr --output LVDS1 --mode 1600x900 --pos 320x0 --rotate normal --output HDMI1 --mode 1920x1080 --pos 0x0 --rotate normal 
xrandr --output HDMI2 --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI1 --off --output DP1 --off --output eDP1 --off --output VIRTUAL1 --off

xcalib -c 
nitrogen --restore
killall conky
conky
exit
